local M = {}
--- A protected require.
-- Require a library inside a protected call so as to not bring down the entire
-- program when library cannot be loaded
-- @param lib_name the name of the library as a string
-- @return return table representing the library.
-- Exits returning nil if library cannot be loaded
-- @usage prequire("module-name")
function M.prequire(lib_name)
	local status_ok, lib = pcall(require, lib_name)
	if status_ok then
		return lib
	else
		vim.notify(string.format("Error loading %s", lib))
		return
	end
end

return M
