local utils = require("user.utils")
local blankline = utils.prequire("indent_blankline")
local column = utils.prequire("virt-column")

-- Indentation guides plugin
blankline.setup({
	char = "¦",
	buftype_exclude = { "terminal" },
})

-- Custom line width guide plugin
column.setup({
	char = "│",
})
