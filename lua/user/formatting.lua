-- Removes trailing whitespace from all lines in the document
local function strip_trailing_whitespace()
    local cursor_position = vim.api.nvim_win_get_cursor(0)
    vim.cmd([[%s/\s\+$//e]])
    vim.api.nvim_win_set_cursor(0, cursor_position)
end

vim.api.nvim_create_autocmd("BufWritePre", {
    callback = strip_trailing_whitespace,
})
