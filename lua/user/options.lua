local options = {
	syntax = "on", -- Syntax highlighting
	number = true, -- Line numbers
	showcmd = true, -- Show the commands run at the bottom
	wrap = false, -- Text wrapping
	clipboard = "unnamedplus", -- Use system clipboard. Requires xclip
	smartindent = true, -- Automatically indent next line
	autoindent = true,
	-- Tab width
	tabstop = 4,
	softtabstop = 4,
	shiftwidth = 4,
	expandtab = true,

	mouse = "a", -- Enable mouse in all modes
	scrolloff = 25, -- Keeps a few lines between top and bottom of screen during scroll
	termguicolors = true, -- Required for the gruvbox theme to function properly
	background = "dark", -- Use dark colorscheme when possible
	cursorline = true, -- Highlight the line with cursor
	cursorlineopt = "line",
	showmode = false, -- Turn off display of current mode as status line takes care of this
	colorcolumn = "80", -- Ruler for line width guide
	splitright = true, -- Open new splits to the right
}

for option, value in pairs(options) do
	vim.opt[option] = value
end

-- Set shorter tabs for some document types
local shorter_tab_filetypes = { "plaintex", "tex", "xml", "html" }
local function set_shorter_tabs()
	vim.schedule(function()
		vim.opt_local.tabstop = 2
		vim.opt_local.softtabstop = 2
		vim.opt_local.shiftwidth = 2
	end)
end

vim.api.nvim_create_autocmd("Filetype", {
	pattern = shorter_tab_filetypes,
	callback = set_shorter_tabs,
})

-- Disable automatic commenting of next line
local function disable_automatic_commenting()
	vim.opt.formatoptions:remove({ "c", "r", "o" })
end

vim.api.nvim_create_autocmd({ "BufRead", "BufNewFile", "BufEnter" }, {
	pattern = "*",
	callback = disable_automatic_commenting,
})
