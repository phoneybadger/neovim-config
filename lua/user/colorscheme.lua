local colorscheme = "onedark"
local transparent_background = false

-- for vscode colorscheme
vim.g.vscode_style = "dark"

-- for onedark colorscheme
local utils = require("user.utils")
local onedark = utils.prequire("onedark")
onedark.setup({
	style = "warmer",
})

-- Set colorscheme and handle errors
local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
	vim.notify("colorscheme " .. colorscheme .. " not found!")
	return
end

if transparent_background then
	vim.cmd([[highlight Normal guibg=none]])
end
