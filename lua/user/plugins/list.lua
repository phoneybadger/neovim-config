vim.cmd([[packadd packer.nvim]])

local utils = require("user.utils")
local packer = utils.prequire("packer")

return packer.startup(function(use)
	use({ "wbthomason/packer.nvim" }) -- Package manager

	use({
		"kyazdani42/nvim-tree.lua", -- File Tree
		requires = "kyazdani42/nvim-web-devicons",
	})

	use({ "hoob3rt/lualine.nvim", requires = "kyazdani42/nvim-web-devicons" }) -- Status Line

	use({
		"romgrk/barbar.nvim", -- Tab Line
		requires = { "kyazdani42/nvim-web-devicons" },
	})

	use({
		"terrortylor/nvim-comment", -- Commenting
		config = function()
			require("nvim_comment").setup()
		end,
	})

	use({ "ygm2/rooter.nvim" }) -- Change working directory to project root

	use({
		"nvim-treesitter/nvim-treesitter", -- Treesitter AST parser.
		run = ":TSUpdate",
	})

	use({
		"nvim-telescope/telescope.nvim", -- Fuzzy finder
		requires = { { "nvim-lua/plenary.nvim" } },
	})

	use({ "windwp/nvim-autopairs" }) -- Automatically close brackets

	--colorschemes
	use({
		"ellisonleao/gruvbox.nvim",
		requires = { "rktjmp/lush.nvim" },
	})
	use({ "Mofiqul/vscode.nvim" })
	use({ "navarasu/onedark.nvim" })

	use({ "neovim/nvim-lspconfig" }) -- LSP and autocompletion
	use({
		"hrsh7th/nvim-cmp",
		requires = {
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-buffer",
			"L3MON4D3/LuaSnip",
		},
	})
	use({ "hrsh7th/cmp-path" }) -- Adds file paths to completion
	use({ "hrsh7th/cmp-nvim-lua" }) -- Adds lua vim API to suggestions
	use({ "saadparwaiz1/cmp_luasnip" }) -- Adds luasnip snippet completions
	use({ "onsails/lspkind-nvim" }) -- Add source names to completion suggestions
	use({ "tami5/lspsaga.nvim" }) -- Wrapper for nice LSP features
	use({ "jose-elias-alvarez/null-ls.nvim" }) -- Use external formatters, diagnostics etc with LSP

	use({ "lukas-reineke/indent-blankline.nvim" }) -- Indentation guides
	use({ "lukas-reineke/virt-column.nvim" }) -- Use custom character for line width guide

	use({ -- Show git diffs and perform some git operations
		"lewis6991/gitsigns.nvim",
		requires = {
			"nvim-lua/plenary.nvim",
		},
	})

	use({ "akinsho/toggleterm.nvim" }) -- Terminal

	use({ "arrufat/vala.vim" }) --Syntax highlighting for vala

	use({ "folke/which-key.nvim" })

    use({ "windwp/nvim-ts-autotag" })

	-- Automatically set up your configuration after cloning packer.nvim
	-- Put this at the end after all plugins
	if PACKER_BOOTSTRAP then
		require("packer").sync()
	end
end)
