-- Requires TreeSitter html parser to be installed
local utils = require("user.utils")
local autotag = utils.prequire("nvim-ts-autotag")
autotag.setup()
