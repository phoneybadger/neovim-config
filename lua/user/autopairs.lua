local utils = require("user.utils")
local autopairs = utils.prequire("nvim-autopairs")
autopairs.setup()
