-- Format for keybindings

-- Custom wrapper to nvim_set_keymap with alternate defaults
local function map(mode, lhs, rhs, opts)
	local options = { noremap = true, silent = true }
	-- if alternate options are provided, override default preference
	if opts then
		options = vim.tbl_extend("force", options, opts)
	end
	vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

-- Setting leader key
map("n", "<Space>", "<NOP>")
vim.g.mapleader = " "

map("n", "<Leader>h", ":set hlsearch!<CR>", { desc = "Remove highlights" })
map("n", "<Leader>n", ":NvimTreeToggle<CR>", { desc = "Toggle file tree" })

-- Ctrl + /, for some reason _ represents /
map("", "<C-_>", ":CommentToggle<CR>", { desc = "Toggle comments" })

----------------------------------------------------
-- INDENT AND UNINDENT FILES
----------------------------------------------------
map("v", ">", ">gv") -- Donot exit visual mode after indenting
map("v", "<", "<gv")

----------------------------------------------------
-- BUFFER/TAB NAVIGATION
----------------------------------------------------
map("n", "<S-h>", ":BufferPrevious<CR>", { desc = "Previous buffer" })
map("n", "<S-l>", ":BufferNext<CR>", { desc = "Next buffer" })
map("n", "<A-c>", ":bdelete<CR>", { desc = "Close buffer" })

----------------------------------------------------
-- MOVE LINES UP/DOWN
----------------------------------------------------
map("", "<A-j>", ":m .+1<CR>==", { desc = "Move line down" })
map("", "<A-k>", ":m .-2<CR>==", { desc = "Move line up" })

map("", "<A-Down>", ":m .+1<CR>==", { desc = "Move line down" })
map("", "<A-UP>", ":m .-2<CR>==", { desc = "Move line up" })

----------------------------------------------------
-- FUZZY FIND
----------------------------------------------------
map("n", "<C-p>", "<cmd>lua require('user.fuzzy_finder').find_files_dropdown()<cr>", { desc = "Find files" })
map("n", "<A-p>", ":Telescope live_grep<CR>", { desc = "Global search" })

----------------------------------------------------
-- LSP
----------------------------------------------------
map("n", "<S-k>", ":Lspsaga hover_doc<CR>", { desc = "LSP documentation" })
map("n", "<Leader>r", ":Lspsaga rename<CR>", { desc = "LSP rename" })
map("n", "gd", ":lua vim.lsp.buf.definition()<CR>", { desc = "LSP go to definition" })
map("n", "<Leader>d", ":Lspsaga show_line_diagnostics<CR>", { desc = "LSP Diagnostics" })

-----------------------------------------------------
-- Splits
-----------------------------------------------------
map("n", "<C-h>", ":wincmd h<CR>", { desc = "Move to left split" })
map("n", "<C-l>", ":wincmd l<CR>", { desc = "Move to right split" })

-----------------------------------------------------
-- Snippets
-----------------------------------------------------
map("i", "<C-j>", "<cmd>lua require('user.snippets').jump_to_next_field()<CR>", { desc = "Snippet jump to next" })
map("s", "<C-j>", "<cmd>lua require('user.snippets').jump_to_next_field()<CR>", { desc = "Snippet jump to next" })
map(
	"i",
	"<C-k>",
	"<cmd>lua require('user.snippets').jump_to_previous_field()<CR>",
	{ desc = "Snippet jump to previous" }
)
map(
	"s",
	"<C-k>",
	"<cmd>lua require('user.snippets').jump_to_previous_field()<CR>",
	{ desc = "Snippet jump to previous" }
)

-----------------------------------------------------
-- Git
-----------------------------------------------------
map("v", "<leader>sh", "<cmd>:Gitsigns stage_hunk<CR>", { desc = "Git stage hunk" })
map("n", "<leader>sh", "<cmd>:Gitsigns stage_hunk<CR>", { desc = "Git stage hunk" })
