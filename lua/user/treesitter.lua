-- Treesitter AST parser
require("nvim-treesitter.configs").setup({
	-- Modules and its options go here
	highlight = { enable = true },
	textobjects = { enable = true },
})
