local utils = require("user.utils")
local null_ls = utils.prequire("null-ls")

local formatting = null_ls.builtins.formatting
local diagnostics = null_ls.builtins.diagnostics

null_ls.setup({
    sources = {
        -- Python
        formatting.black, -- formatter
        formatting.isort, -- sort dependencies
        diagnostics.mypy, -- type checking
        -- Lua
        formatting.stylua,
        -- Go
        formatting.gofmt
    },
})
