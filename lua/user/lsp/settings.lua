-- Changes from default icons for diagnostics
local utils = require("user.utils")
local lspsaga = utils.prequire("lspsaga")
lspsaga.setup({
	error_sign = "",
	warn_sign = "",
	hint_sign = "",
	infor_sign = "",
})
