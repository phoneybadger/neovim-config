local utils = require("user.utils")
local lspconfig = utils.prequire("lspconfig")

local format_on_save_file_paths = {}
-----------------------------------------------------
-- Python language server. Requires python-lsp-server
-----------------------------------------------------
lspconfig.pylsp.setup({
    -- Turn off document formatting as we're using null-ls to perform
    -- formatting using black
    on_attach = function(client)
        client.server_capabilities.document_formatting = false
    end,
})
table.insert(format_on_save_file_paths, "*.py")

-------------------------------------------------------
-- Vala language server. Requires vala-language-server
-------------------------------------------------------
lspconfig.vala_ls.setup({})

-------------------------------------------------------
-- Lua language server. Requires sumneko-lua
-------------------------------------------------------
require 'lspconfig'.lua_ls.setup {
    settings = {
        Lua = {
            runtime = {
                -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
                version = 'LuaJIT',
            },
            diagnostics = {
                -- Get the language server to recognize the `vim` global
                globals = { 'vim' },
            },
            workspace = {
                -- Make the server aware of Neovim runtime files
                library = vim.api.nvim_get_runtime_file("", true),
            },
            -- Do not send telemetry data containing a randomized but unique identifier
            telemetry = {
                enable = false,
            },
        },
    },
}
table.insert(format_on_save_file_paths, "*.lua")

-------------------------------------------------------
-- Latex Language server. Requires texlab
-------------------------------------------------------
lspconfig.texlab.setup({
    filetypes = { "tex", "plaintex", "bib" },
    settings = {
        texlab = {
            auxDirectory = "out",
            -- Build the files on save. Requires tectonic
            build = {
                executable = "tectonic",
                args = {
                    "-X",
                    "compile",
                    "%f",
                    -- tuck all auxillary files neatly into other directory
                    "--outdir",
                    "out",
                    -- required for completions
                    "--synctex",
                    "--keep-logs",
                    "--keep-intermediates",
                },
                onSave = true,
            },
        },
    },
})

-------------------------------------------------------
-- Rust Language server, requires rust analyzer
-------------------------------------------------------
lspconfig.rust_analyzer.setup({})
table.insert(format_on_save_file_paths, "*.rs")

-------------------------------------------------------
-- C Language server. Requires clangd.
-------------------------------------------------------
lspconfig.clangd.setup({})

-------------------------------------------------------
-- Go Language server. Requires gopls.
-------------------------------------------------------
lspconfig.gopls.setup({})

-------------------------------------------------------
-- Format on save
-------------------------------------------------------
local function format_on_save()
    vim.lsp.buf.format()
end

vim.api.nvim_create_autocmd("BufWritePre", {
    pattern = format_on_save_file_paths,
    callback = format_on_save,
})
