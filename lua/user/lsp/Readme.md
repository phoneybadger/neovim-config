# LSP 
Language server setups for different languages for diagnostics, formatting etc.

## Python
requires `python-lsp-server`. Using `pyflakes` and `pycodestyle` extensions for
diagnostics and style checking respectively.

using `null-ls` neovim plugin to format on save using `black` and sort dependencies
alphabetically using `isort`. Using `mypy` along with `null-ls` to provide 
type checking diagnostics.

using `pipenv` run this command to install as development dependencies in a
virtual environment
```
pipenv install python-lsp-server mypy black isort pyflakes pycodestyle --dev
```
