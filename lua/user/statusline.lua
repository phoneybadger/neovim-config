local utils = require("user.utils")
local lualine = utils.prequire("lualine")

lualine.setup({
	options = {
		theme = "onedark",
		section_separators = { "", "" },
		component_separators = { "", "" },
	},
	tabline = {},
	extensions = { "nvim-tree" },
})
