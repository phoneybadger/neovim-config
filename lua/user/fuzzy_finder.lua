local M = {}

local utils = require("user.utils")
local telescope = utils.prequire("telescope")
telescope.setup({
	defaults = {
		prompt_prefix = "  ",
	},
})

function M.find_files_dropdown()
	local builtins = require("telescope.builtin")
	local themes = require("telescope.themes")
	builtins.find_files(themes.get_dropdown({ previewer = false }))
end

return M
