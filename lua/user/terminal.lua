local utils = require("user.utils")
local toggleterm = utils.prequire("toggleterm")

toggleterm.setup({
	size = 15,
	open_mapping = [[<C-t>]],
})
