# Neovim lua configuration

Lua configuration files for neovim. Clone this repo into `~/.config/nvim` or
clone it somewhere else and symlink it to get the configuration working. As of 
now packer should install automatically and install all the plugins. Treesitter
grammars and lsp servers still have to be manually installed.
