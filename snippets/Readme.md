# Snippets
Custom snippets made for use with neovim. Using the [luasnip](https://github.com/L3MON4D3/LuaSnip)
snippet engine. These are JSON snippets in the VSCode JSON snippet format. The `package.json` contains
an index of all the snippets and their paths and the `languages` directory contains all the snippets
sorted by language

